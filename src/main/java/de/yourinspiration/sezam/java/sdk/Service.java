/*
 * The MIT License
 *
 * Copyright 2014 Yourinpiration.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.yourinspiration.sezam.java.sdk;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author Marcel Härle
 */
public class Service {

    private static final Log LOG = LogFactory.getLog(Service.class);

    private final LoadingCache<String[], Account> cache;

    private final Credentials credentials;
    private String digest;

    private static final String CONFIG_FILE_NAME = "sezam.properties";
    private static final String APPLICATION_ID_PROPERTY = "id";
    private static final String KEY_PROPERTY = "key";
    private static final String DIGEST_PROPERTY = "digest";
    private static final String JSON_TYPE = "application/json";

    private static final String DEFAULT_URL = "https://sezam.yourinspiration.de/rest/accounts";
    private static final int DEFAULT_OBJECT_ID_LENGTH = 24;

    private final boolean useCache;

    /**
     * Creates a new service instance with cache enabled.
     */
    public Service() {
        this(true);
    }

    /**
     * Creates a new service instance.
     *
     * @param useCache whether cache is enabled
     */
    public Service(boolean useCache) {
        this.useCache = useCache;
        this.credentials = getCredentials();
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .build(new AccountCacheLoader());
    }

    /**
     * Save the given account.
     *
     * @param account the account to be saved, must not be null
     */
    public void save(Account account) {
        Preconditions.checkNotNull(account);
        try {
            if (account.getId() == null || "".equals(account.getId())) {
                HttpPost request = new HttpPost(DEFAULT_URL);

                StringEntity input = new StringEntity(
                        createJsonFromAccount(account));
                input.setContentType(JSON_TYPE);
                request.setEntity(input);
                request.setHeaders(HeaderUtils.createHeaders(credentials));

                CloseableHttpClient httpClient = HttpClients.createDefault();

                try (CloseableHttpResponse response = httpClient.execute(request)) {
                    if (response.getStatusLine().getStatusCode() != 201
                            && response.getStatusLine().getStatusCode() != 200) {
                        final String msg = "error creating account";
                        throw new RuntimeException(msg);
                    }

                    final String responseContent = readContentFromResponse(response);
                    final Account created = createAccountFromJson(responseContent);

                    account.setId(created.getId());
                }
            } else {
                HttpPut request = new HttpPut(DEFAULT_URL);

                StringEntity input = new StringEntity(
                        createJsonFromAccount(account));
                input.setContentType(JSON_TYPE);
                request.setEntity(input);
                request.setHeaders(HeaderUtils.createHeaders(credentials));

                CloseableHttpClient httpClient = HttpClients.createDefault();

                try (CloseableHttpResponse response = httpClient.execute(request)) {
                    readContentFromResponse(response);

                    if (response.getStatusLine().getStatusCode() != 200) {
                        final String msg = "error saving account";
                        throw new RuntimeException(msg);
                    }
                }
            }

        } catch (IOException | IllegalStateException exception) {
            final String msg = "error saving account";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);
        }
    }

    /**
     * Finds the account for the given id.
     *
     * @param id the id of the account
     * @return returns null if no such account was found
     */
    public Account findById(final String id) {
        checkValidId(id);

        if (useCache) {
            return cache.getUnchecked(new String[]{"id", id});
        }

        try {
            HttpGet request = new HttpGet(DEFAULT_URL + "/" + id);

            request.setHeaders(HeaderUtils.createHeaders(credentials));

            CloseableHttpClient httpClient = HttpClients.createDefault();

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                return createAccountFromJson(readContentFromResponse(response));
            }
        } catch (IOException | IllegalStateException exception) {
            final String msg = "error finding account by id";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);
        }
    }

    /**
     * Finds the account for the given username.
     *
     * @param username the username of the account
     * @return returns null if no such account was found
     */
    public Account findByUsername(final String username) {
        Preconditions.checkNotNull(username);

        if (useCache) {
            return cache.getUnchecked(new String[]{"username", username});
        }
        try {
            HttpGet request = new HttpGet(DEFAULT_URL + "/username/" + username);

            request.setHeaders(HeaderUtils.createHeaders(credentials));

            CloseableHttpClient httpClient = HttpClients.createDefault();

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                return createAccountFromJson(readContentFromResponse(response));
            }
        } catch (IOException | IllegalStateException exception) {
            final String msg = "error finding account by username";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);
        }
    }

    /**
     * Finds the account for the given email.
     *
     * @param email the email of the account
     * @return returns null if no such account was found
     */
    public Account findByEmail(final String email) {
        Preconditions.checkNotNull(email);

        if (useCache) {
            return cache.getUnchecked(new String[]{"email", email});
        }
        try {
            HttpGet request = new HttpGet(DEFAULT_URL + "/email/" + email);

            request.setHeaders(HeaderUtils.createHeaders(credentials));

            CloseableHttpClient httpClient = HttpClients.createDefault();

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                return createAccountFromJson(readContentFromResponse(response));
            }
        } catch (IOException | IllegalStateException exception) {
            final String msg = "error finding account by email";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);
        }
    }

    /**
     * Retrieves all accounts for the current application.
     *
     * @return returns an empty list if no account were found
     */
    public List<Account> findAll() {
        try {
            HttpGet request = new HttpGet(DEFAULT_URL);

            request.setHeaders(HeaderUtils.createHeaders(credentials));

            CloseableHttpClient httpClient = HttpClients.createDefault();

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                return createAccountsFromJson(readContentFromResponse(response));
            }
        } catch (IOException | IllegalStateException exception) {
            final String msg = "error finding all accounts";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);
        }
    }

    /**
     * Deletes all accounts for the current application.
     */
    public void deleteAll() {
        try {
            HttpDelete request = new HttpDelete(DEFAULT_URL);

            request.setHeaders(HeaderUtils.createHeaders(credentials));

            CloseableHttpClient httpClient = HttpClients.createDefault();

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                readContentFromResponse(response);
            }
        } catch (IOException exception) {
            final String msg = "error deleting all accounts";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);
        }
    }

    /**
     * Deletes the given account.
     *
     * @param account the account to be deleted, must not be null
     */
    public void delete(Account account) {
        Preconditions.checkNotNull(account);
        checkValidId(account.getId());

        try {
            HttpDelete request = new HttpDelete(DEFAULT_URL + "/" + account.getId());

            StringEntity input = new StringEntity(
                    createJsonFromAccount(account));
            input.setContentType(JSON_TYPE);
            request.setHeaders(HeaderUtils.createHeaders(credentials));

            CloseableHttpClient httpClient = HttpClients.createDefault();

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                readContentFromResponse(response);

                if (response.getStatusLine().getStatusCode() != 200) {
                    final String msg = "error deleting account";
                    throw new RuntimeException(msg);
                }
            }
        } catch (RuntimeException | IOException exception) {
            final String msg = "error deleting account";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);

        }
    }

    /**
     * Resets the password for the given account.
     *
     * @param account the account for password reset, must not be null
     */
    public void resetPassword(Account account) {
        Preconditions.checkNotNull(account);
        checkValidId(account.getId());

        try {
            HttpPut request = new HttpPut(DEFAULT_URL + "/passwordReset");

            StringEntity input = new StringEntity(
                    createJsonFromAccount(account));
            input.setContentType(JSON_TYPE);
            request.setEntity(input);
            request.setHeaders(HeaderUtils.createHeaders(credentials));

            CloseableHttpClient httpClient = HttpClients.createDefault();

            try (CloseableHttpResponse response = httpClient.execute(request)) {

                final String responseContent = readContentFromResponse(response);
                final Account updated = createAccountFromJson(responseContent);

                account.setPassword(updated.getPassword());
            }
        } catch (IOException | IllegalStateException exception) {
            final String msg = "error resetting password";
            LOG.info(msg, exception);
            throw new RuntimeException(msg, exception);
        }
    }

    /**
     * Authenticate for the given username.
     *
     * @param username the username
     * @param password the password
     * @return return <code>true</code> if the authentication succeeded,
     * otherwise <code>false</code>
     */
    public boolean authenticateByUsername(final String username,
            final String password) {
        Preconditions.checkNotNull(username);
        Preconditions.checkNotNull(password);

        final Account account = findByUsername(username);

        if (account != null) {
            return checkPassword(password, account);
        }

        return false;
    }

    /**
     * Authenticate for the given email.
     *
     * @param email the email
     * @param password the password
     * @return return <code>true</code> if the authentication succeeded,
     * otherwise <code>false</code>
     */
    public boolean authenticateByEmail(final String email,
            final String password) {
        Preconditions.checkNotNull(email);
        Preconditions.checkNotNull(password);

        final Account account = findByEmail(email);

        if (account != null) {
            return checkPassword(password, account);
        }

        return false;
    }

    private Credentials getCredentials() {
        LOG.warn("This SDK is not meant to be used in production systems.");
        try {
            final PropertiesConfiguration config = new PropertiesConfiguration(
                    CONFIG_FILE_NAME);
            config.setThrowExceptionOnMissing(Boolean.TRUE);

            final String applicationId = config
                    .getString(APPLICATION_ID_PROPERTY);
            final String key = config.getString(KEY_PROPERTY);
            digest = config.getString(DIGEST_PROPERTY);

            return new Credentials(applicationId, key);
        } catch (ConfigurationException e) {
            throw new RuntimeException("error reading credentials from "
                    + CONFIG_FILE_NAME + ": " + e.getMessage(), e);
        }
    }

    private String createJsonFromAccount(Account account) {
        return ("{" + "'id':'" + account.getId() + "'," + "'username':'"
                + account.getUsername() + "'," + "'email':'"
                + account.getEmail() + "'," + "'password':'"
                + account.getPassword() + "'," + "'enabled':"
                + account.isEnabled() + "," + "'roles':'" + account.getRoles()
                + "'" + "}").replaceAll("'", "\"");
    }

    private String readContentFromResponse(CloseableHttpResponse response)
            throws IllegalStateException, IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response
                .getEntity().getContent()));

        StringBuilder content = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            content.append(line);
        }

        return content.toString();
    }

    private boolean checkPassword(String password, Account account) {
        if ("BCRYPT".equalsIgnoreCase(digest)) {
            return BCrypt.checkpw(password, account.getPassword());
        } else {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance(digest);
                byte[] hash = messageDigest.digest(password.getBytes());
                StringBuilder hexString = new StringBuilder();
                for (int i = 0; i < hash.length; i++) {
                    if ((0xff & hash[i]) < 0x10) {
                        hexString.append("0").append(Integer.toHexString((0xFF & hash[i])));
                    } else {
                        hexString.append(Integer.toHexString(0xFF & hash[i]));
                    }
                }
                return hexString.toString().equals(account.getPassword());
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("error checking password: "
                        + e.getMessage(), e);
            }
        }
    }

    private Account createAccountFromJson(final String json) {
        final JsonElement jsonEle = new JsonParser().parse(json);

        if (!jsonEle.getClass().equals(JsonNull.class)) {
            final JsonObject jsonObject = jsonEle.getAsJsonObject();

            final Account account = new Account();

            account.setEmail(jsonObject.get("email").getAsString());
            account.setEnabled(jsonObject.get("enabled").getAsBoolean());
            account.setId(jsonObject.get("id").getAsString());
            account.setPassword(jsonObject.get("password").getAsString());
            account.setRoles(jsonObject.get("roles").getAsString());
            account.setUsername(jsonObject.get("username").getAsString());

            return account;
        } else {
            return null;
        }
    }

    private List<Account> createAccountsFromJson(final String json) {
        final List<Account> accounts = new ArrayList<>();

        final JsonElement jsonEle = new JsonParser().parse(json);

        if (!jsonEle.getClass().equals(JsonNull.class)) {
            JsonArray jsonArray = jsonEle.getAsJsonArray();

            for (int i = 0; i < jsonArray.size(); i++) {
                accounts.add(createAccountFromJson(jsonArray.get(i)
                        .getAsJsonObject().toString()));
            }
        }

        return accounts;
    }

    private void checkValidId(String id) {
        if (id == null || "".equals(id)) {
            throw new IllegalArgumentException("id must not be null or empty");
        }
        if (id.length() != DEFAULT_OBJECT_ID_LENGTH) {
            throw new IllegalArgumentException("id is not a valid Object ID");
        }
    }

    private class AccountCacheLoader extends CacheLoader<String[], Account> {

        @Override
        public Account load(String[] condition) {
            if (condition.length != 2) {
                throw new IllegalArgumentException("invalid condition");
            }
            if (condition[0].equals("username")) {
                try {
                    HttpGet request = new HttpGet(DEFAULT_URL + "/username/" + condition[1]);

                    request.setHeaders(HeaderUtils.createHeaders(credentials));

                    CloseableHttpClient httpClient = HttpClients.createDefault();

                    try (CloseableHttpResponse response = httpClient.execute(request)) {
                        return createAccountFromJson(readContentFromResponse(response));
                    }
                } catch (IOException | IllegalStateException exception) {
                    final String msg = "error finding account by username";
                    LOG.info(msg, exception);
                    throw new RuntimeException(msg, exception);
                }
            }
            if (condition[0].equals("email")) {
                try {
                    HttpGet request = new HttpGet(DEFAULT_URL + "/email/" + condition[1]);

                    request.setHeaders(HeaderUtils.createHeaders(credentials));

                    CloseableHttpClient httpClient = HttpClients.createDefault();

                    try (CloseableHttpResponse response = httpClient.execute(request)) {
                        return createAccountFromJson(readContentFromResponse(response));
                    }
                } catch (IOException | IllegalStateException exception) {
                    final String msg = "error finding account by email";
                    LOG.info(msg, exception);
                    throw new RuntimeException(msg, exception);
                }
            }
            if (condition[0].equals("id")) {
                try {
                    HttpGet request = new HttpGet(DEFAULT_URL + "/" + condition[1]);

                    request.setHeaders(HeaderUtils.createHeaders(credentials));

                    CloseableHttpClient httpClient = HttpClients.createDefault();

                    try (CloseableHttpResponse response = httpClient.execute(request)) {
                        return createAccountFromJson(readContentFromResponse(response));
                    }
                } catch (IOException | IllegalStateException exception) {
                    final String msg = "error finding account by id";
                    LOG.info(msg, exception);
                    throw new RuntimeException(msg, exception);
                }
            } else {
                throw new IllegalArgumentException("unknown condition " + condition[0]);
            }
        }
    }

}
