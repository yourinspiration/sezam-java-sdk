/* 
 * The MIT License
 *
 * Copyright 2014 Yourinpiration.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.yourinspiration.sezam.java.sdk;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.message.BasicHeader;

public final class HeaderUtils {

    private static final Log LOG = LogFactory.getLog(HeaderUtils.class);

    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String ACCEPT_HEADER = "Accept";
    private static final String JSON_CONTENT_TYPE = "application/json";

    private HeaderUtils() {

    }

    /**
     * Builds a HTTP header with HTTP-Basic-Authentication header and content
     * type application/json.
     *
     * @param credentials the credentials for HTTP-Basic-Authentication header
     * @return returns HTTP headers
     */
    public static BasicHeader[] createHeaders(final Credentials credentials) {
        final BasicHeader[] headers = new BasicHeader[3];

        final String auth = credentials.getApplicationId() + ":"
                + credentials.getKey();
        final byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset
                .forName("US-ASCII")));

        try {
            final String authHeader = "Basic "
                    + new String(encodedAuth, "UTF-8");
            headers[0] = new BasicHeader("Authorization", authHeader);
        } catch (UnsupportedEncodingException e) {
            LOG.warn("error creating Basic Auth Header", e);
        }

        headers[1] = new BasicHeader(CONTENT_TYPE_HEADER, JSON_CONTENT_TYPE);
        headers[2] = new BasicHeader(ACCEPT_HEADER, JSON_CONTENT_TYPE);

        return headers;
    }

}
